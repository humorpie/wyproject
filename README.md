## 平台介绍
无鱼工时管理系统，是一款轻量级工时记录和管理工具，包括项目管理，工时上报，工时日报，工时统计等功能。

无鱼工时管理系统可通过员工工时上报的方式，来记录项目所花费的工时，帮助企业进行项目工时统计、核算人工成本。实时、动态、真实的展示项目投入。

## 软件架构
JAVA 1.8 +  MYSQL +VUE
本项目基于前后端分离架构：
服务端：springboot
前端：vue

| 模块  | 说明  |
|---|---|
| wuyu-common | 项目公共模块  |
| wuyu-system  | 系统功能的Module模块  |
| wuyu-framework  | 基础框架  |
| wuyu-admin-web  | 系统功能接口服务模块  |
| wuyu-prototype| 系统工时原型Module模块 |



## 功能列表
工时模块

1、我的工时：提交工时、查看详情、修改工时、我的工时、我的统计（填报率）。

2、项目统计：项目日报、项目月报、项目日历、项目工时详情。

3、项目投入：项目成本、项目投入进度、项目工时总计、工时明细、项目详情等。

4、数据统计：工时统计、人员排行、阶段统计、工作内容统计。

5、人员填报率统计：人员填报率排名、填报详情。

6、工时审核：工时审核设置、工时审批。

7、工时归档：工时归档设置、工时归档。

8、消息提醒：工时审核提醒。

管理模块

1、项目管理：创建项目、添加人员、工时设置、状态管理、删除项目。

2、业务管理：加班管理、节假日管理、工作类型管理、审核管理、归档管理。

3、成本管理：成本开关、岗位成本设置、人员成本设置。

4、组织管理：用户管理、角色管理、重置密码、批量删除。

5、职位管理：新增、修改、删除、批量删除等功能。


## 功能截图

#### 首页

![](document/img/1.png)

#### 我的
<table>
    <tr>
        <td>  <img src="document/img/2.png"/>   </td>
        <td>  <img src="document/img/3.png"/>   </td>
    </tr>
    <tr>
        <td> <img src="document/img/13.png"/>  </td>
        <td> <img src="document/img/yuangong01.png"/> </td>
    </tr>
     <tr>
        <td> <img src="document/img/yuangong02.png"/>  </td>
        <td> <img src="document/img/yuangong03.png"/> </td>
    </tr>
</table>

#### 项目统计
<table>
    <tr>
        <td>  <img src="document/img/xm02.png"/>   </td>
        <td>  <img src="document/img/xm01.png"/>   </td>
    </tr>
    <tr>
        <td> <img src="document/img/yuebao01.png"/>  </td>
        <td> <img src="document/img/yuebao02.png"/> </td>
    </tr>
     <tr>
        <td> <img src="document/img/4.png"/>  </td>
        <td> <img src="document/img/5.png"/> </td>
    </tr>
</table>

#### 工时统计
<table>
    <tr>
        <td>  <img src="document/img/6.png"/>   </td>
        <td>  <img src="document/img/7.png"/>   </td>
    </tr>
    <tr>
        <td> <img src="document/img/12.png"/>  </td>
        <td> <img src="document/img/review2.png"/> </td>
    </tr>
     <tr>
        <td> <img src="document/img/4.png"/>  </td>
        <td> <img src="document/img/5.png"/> </td>
    </tr>
</table>





#### 手机app端

<table>
    <tr>
        <td>  <img src="document/appimg/app1.png"/>   </td>
        <td>  <img src="document/appimg/app2.png"/>   </td>
    </tr>
    <tr>
        <td>  <img src="document/appimg/app4.png"/>   </td>
        <td>  <img src="document/appimg/app3.png"/>   </td>
    </tr>
</table>





## 演示
暂无

## 专业版演示

web端：

http://pro.wuyusoft.com


手机端：
http://pro.wuyusoft.com/html5

| #| 角色 |	 账号 | 	密码    |  权限 |
| :--: | :------: | :------: |:------: | ---------- |
|演示环境账号1 |开发者 | 	dev1|	123456|填报工时|
|演示环境账号2 |项目经理 |	pm1  |	123456 |查看工时统计、管理项目人员、工时审核|


## 使用文档
[文档]( https://doc.wuyusoft.com/cost/)

## 部署方式

[部署说明]( https://doc.wuyusoft.com/cost/install.html)

### 
web端默认地址:
http://ip:80

html5/手机端默认地址：
http://ip:80/html5


默认管理账号/密码：
admin/12345678


## faq

~~[相关问题]( https://doc.wuyusoft.com/cost/faq.html) ~~


## 交流
1、QQ群用户交流群
为减少客服小姐姐的工作量以及屏蔽一些骚扰者，加群改为通过公众号获取。
添加公众号 wuyusoft 或者扫码后关注，发送"加群"。
不要加管理员！不要加管理员！不要加管理员！

![](document/doc/qrcode.jpg)


2、程序下载群 
737185971
可直接加入,加入后下载群文件即可。（已经编译好程序包）



## 官网
http://www.wuyusoft.com



## 感谢
该项目基于若依、vue、ele-admin-ui等相关框架，在此进行感谢。
